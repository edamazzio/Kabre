//gcc -o numeric_password_finder numeric_password_finder.c -lm -fopenmp -std=c99
//gcc -o threads_con_sleep threads_con_sleep.c -lm -fopenmp -std=c99


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>
#include <unistd.h>


double find_password_in_range(double password, double first_i, double last_i);
int get_digit_count(double password);

int password_digit_amount;
int num_threads;
int password_was_found = 0;

int main (int argc, char **argv) {

  if (argc != 3){
    fprintf(stderr, "%s", "Two arguments are required! program [time] [#threads]\n");
    return 1;
  }

  struct timeval start, end;
  gettimeofday(&start, NULL);

  #pragma omp parallel
  {
    #pragma omp single
    {
      num_threads = strtol(argv[2],NULL,10);
      omp_set_num_threads(num_threads);
    }
    int current_thread = omp_get_thread_num();
    for(int i=0;i<num_threads;i++){
      if(current_thread == i){
        sleep(strtol(argv[1], NULL, 10));
      }
    }
  }

  gettimeofday(&end, NULL);
  double delta = ((end.tv_sec  - start.tv_sec) * 1000000u +
         end.tv_usec - start.tv_usec) / 1.e6;

  printf("Program took %f seconds to execute \n", delta);

  return 0;
}


double find_password_in_range(double password, double first_i, double last_i){
  double found_password = -1;
  printf("Loop from %f to %f\n", first_i, last_i);
  for(first_i;first_i<last_i;first_i++){
      //printf("%d: Looking for password... %.0f\n",omp_get_thread_num(),first_i);
      if(first_i == password){
        found_password = first_i;
        password_was_found = 1;
      }
      if(password_was_found){
        break;
      }
  }
  return found_password;
}

int get_digit_count(double password){
  int digit_count = 1;
  while (password / 10 >= 1){
    digit_count++;
    password /= 10;
  }
  return digit_count;
}















//
